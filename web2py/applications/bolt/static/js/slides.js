$(document).ready(function(){
  var cposition = 0;
  var swidth = 620;
  var slides = $('.slide');
  var length= slides.length;
  $('#container').css('overflow', 'hidden');
  slides.wrapAll('<div id="slideInner"></div>').css({'float' : 'left','width' : swidth});
  $('#slideInner').css('width', swidth * length);
  $('#slideshow').prepend('<span class="control" id="leftControl">Clicking moves left</span>').append('<span class="control" id="rightControl">Clicking moves right</span>');
  manageControls(cposition);
  $('.control').bind('click', function(){
	cposition = ($(this).attr('id')=='rightControl') ? cposition+1 : cposition-1;
    manageControls(cposition);
    $('#slideInner').animate({
      'marginLeft' : swidth*(-cposition)
    });
  });
 function manageControls(position){
	if(position==0){ $('#leftControl').hide() } else{ $('#leftControl').show() }
    if(position==length-1){ $('#rightControl').hide() } else{ $('#rightControl').show() }
  }	
});